﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Voyageur.Models
{
    public class EditModels
    {
        [Key]
        public int ID { get; set; } 

        [Display(Name = "Ad Soyad")]
        public string Name { get; set; }

        [Display(Name = "Adres")]
        public string Address { get; set; }

        [Phone]
        [Display(Name = "Telefon Numarası")]
        public string Number { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Duğum Günü")]
        public string BirthDay { get; set; }

        [Display(Name = "Kullanıcı Adı")]
        public string UserName { get; set; }



    }
}