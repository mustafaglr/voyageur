﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Voyageur.Controllers
{
    public class EditController : Controller
    {
        // GET: Edit
        public ActionResult Index()
        {
            return View();
        }

        public class EditUserViewModel
        {

            [Display(Name = "Name Surname")]
            public string Name { get; set; }

            [Display(Name = "Address")]
            public string Address { get; set; }

            [Required]
            [Phone]
            [Display(Name = "Phone Number")]
            public string Number { get; set; }

            [DataType(DataType.Date)]
            [Required]
            [Display(Name = "Birth Day")]
            public string BirthDay { get; set; }
        }
    }
}