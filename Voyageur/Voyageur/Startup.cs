﻿using Microsoft.Owin;
using Owin;
[assembly: OwinStartupAttribute(typeof(Voyageur.Startup))]
namespace Voyageur
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
