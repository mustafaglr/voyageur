﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Voyageur.Models;

namespace Voyageur.Context
{
    public class UyeContext:DbContext
    {
        public DbSet<EditModels> Edit { get; set; }
        public DbSet<ShareModels> Share { get; set; }

        public UyeContext(): base("UyeContext")
        {
        }
    }
}