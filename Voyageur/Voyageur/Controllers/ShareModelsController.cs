﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Voyageur.Context;
using Voyageur.Models;

namespace Voyageur.Controllers
{
    [Authorize]

    public class ShareModelsController : Controller
    {
        private UyeContext db = new UyeContext();

        [AllowAnonymous]
        // GET: ShareModels
        public ActionResult Index()
        {
            return View(db.Share.ToList());
        }


        // GET: ShareModels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ShareModels shareModels = db.Share.Find(id);
            if (shareModels == null)
            {
                return HttpNotFound();
            }
            return View(shareModels);
        }

        public ActionResult Create()
        {
            return View();
        }

        // POST: ShareModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Image,Name,Address,Comment,Point,Kategori")] ShareModels shareModels)
        {
            if (ModelState.IsValid)
            {
                db.Share.Add(shareModels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(shareModels);
        }

        // GET: ShareModels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ShareModels shareModels = db.Share.Find(id);
            if (shareModels == null)
            {
                return HttpNotFound();
            }
            return View(shareModels);
        }

        // POST: ShareModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Image,Name,Address,Comment,Point,Kategori")] ShareModels shareModels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(shareModels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(shareModels);
        }

        // GET: ShareModels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ShareModels shareModels = db.Share.Find(id);
            if (shareModels == null)
            {
                return HttpNotFound();
            }
            return View(shareModels);
        }

        // POST: ShareModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ShareModels shareModels = db.Share.Find(id);
            db.Share.Remove(shareModels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
