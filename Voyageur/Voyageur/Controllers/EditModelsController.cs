﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Voyageur.Context;
using Voyageur.Models;

namespace Voyageur.Controllers
{
    [Authorize]
    public class EditModelsController : Controller
    {
        private UyeContext db = new UyeContext();

        // GET: EditModels
        public ActionResult Index()
        {
            return View(db.Edit.Where(x => x.UserName == User.Identity.Name).ToList());

        }

        // GET: EditModels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EditModels editModels = db.Edit.Find(id);
            if (editModels == null)
            {
                return HttpNotFound();
            }
            return View(editModels);
        }

        // GET: EditModels/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EditModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Address,Number,BirthDay,UserName")] EditModels editModels)
        {

            if (ModelState.IsValid)
            {
                db.Edit.Add(editModels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(editModels);
        }

        // GET: EditModels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EditModels editModels = db.Edit.Find(id);
            if (editModels == null)
            {
                return HttpNotFound();
            }
            return View(editModels);
        }

        // POST: EditModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Address,Number,BirthDay,UserName")] EditModels editModels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(editModels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(editModels);
        }

        // GET: EditModels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EditModels editModels = db.Edit.Find(id);
            if (editModels == null)
            {
                return HttpNotFound();
            }
            return View(editModels);
        }

        // POST: EditModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EditModels editModels = db.Edit.Find(id);
            db.Edit.Remove(editModels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

      
    }
}
